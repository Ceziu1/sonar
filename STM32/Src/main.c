/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "vl53l0x_api.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
//typedef uint8_t TOF_Status;
//
//typedef enum tof_range_profile {
//	HIGH_ACCURACY,
//	HIGH_SPEED,
//	LONG_RANGE
//} tof_range_profile;
//
//typedef enum tof_device_mode {
//	SINGLE,
//	CONTINOUS
//} tof_device_mode;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define TOF_CONFIG_NONE		((TOF_Status)	0)
//#define TOF_INIT 			((TOF_Status)	1)
//#define TOF_ACCURACY 		((TOF_Status)	2)
//#define TOF_SPEED			((TOF_Status)	4)
//#define TOF_DISTANCE 		((TOF_Status)	8)
//#define TOF_CONTINOUS 		((TOF_Status)	16)
//#define TOF_SINGLE	 		((TOF_Status)	32)
//#define TOF_START	 		((TOF_Status)	64)
//#define TOF_MEASURE	 		((TOF_Status)	128)

#define BUFFER_SIZE 30

#define SERVO_LEFT 1425
#define SERVO_STOP 1500
#define SERVO_RIGHT 1575
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* TOF VARIABLES */
//TOF_Status tof1_config = 0;
uint8_t tof1_data_ready;
VL53L0X_Dev_t tof1;
VL53L0X_RangingMeasurementData_t tof1_data;

/* VOLATILE VARIABLES */
volatile uint8_t buffer_counter = 0;
volatile uint8_t tx_flag = 0;

/* REST VARIABLES */
uint8_t buffer[BUFFER_SIZE];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */
void command_parser(uint8_t *buffer);
//void tof_init(VL53L0X_Dev_t *tof);
//void tof_set_range_profile(VL53L0X_Dev_t *tof, tof_range_profile profile);
//void tof_set_device_mode(VL53L0X_Dev_t *tof, tof_device_mode mode);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start(&htim1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);

  HAL_UART_Receive_IT(&huart2, buffer, 1);


  uint32_t spadCount;
  uint8_t isAperture, VhvSettings, phaseCall;

  tof1.I2cDevAddr = 0x52;
  VL53L0X_Error Status = 0;
  Status += VL53L0X_ResetDevice(&tof1);
  Status += VL53L0X_DataInit( &tof1 );
  Status += VL53L0X_StaticInit( &tof1 );
  Status += VL53L0X_PerformRefSpadManagement(&tof1, &spadCount, &isAperture);
  Status += VL53L0X_PerformRefCalibration(&tof1, &VhvSettings, &phaseCall);
  Status += VL53L0X_SetDeviceMode(&tof1, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
  Status += VL53L0X_SetMeasurementTimingBudgetMicroSeconds(&tof1, 200000);

  VL53L0X_StartMeasurement( &tof1 );
  HAL_UART_Transmit( &huart2, (uint8_t *)"GPIO: TOF\n", 10, 100);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
//	  VL53L0X_GetMeasurementDataReady(&tof1, &tof1_data_ready);
//	  if(tof1_data_ready == 0x1){
//		  	VL53L0X_GetRangingMeasurementData(&tof1, &tof1_data);
//			sprintf( (char*)buffer, "VL:%d, %d, %.2f, %.2f\n",tof1_data.RangeStatus, tof1_data.RangeMilliMeter,
//					  ( tof1_data.SignalRateRtnMegaCps / 65536.0 ), tof1_data.AmbientRateRtnMegaCps / 65336.0 );
//			HAL_UART_Transmit(&huart2, buffer, strlen((char*)buffer), 200);
//			VL53L0X_ClearInterruptMask(&tof1, VL53L0X_REG_SYSTEM_INTERRUPT_CLEAR);
//	  }

//	  if( tof1_config ){
//		  if((tof1_config & TOF_INIT) == TOF_INIT)
//			  tof_init(&tof1);
//		  else if ((tof1_config & TOF_ACCURACY) == TOF_ACCURACY)
//			  tof_set_range_profile(&tof1, HIGH_ACCURACY);
//		  else if ((tof1_config & TOF_SPEED) == TOF_SPEED)
//			  tof_set_range_profile(&tof1, HIGH_SPEED);
//		  else if ((tof1_config & TOF_DISTANCE) == TOF_DISTANCE)
//			  tof_set_range_profile(&tof1, LONG_RANGE);
//		  else if ((tof1_config & TOF_CONTINOUS) == TOF_CONTINOUS)
//			  tof_set_device_mode(&tof1, CONTINOUS);
//		  else if ((tof1_config & TOF_SINGLE) == TOF_SINGLE)
//			  tof_set_device_mode(&tof1, SINGLE);
//		  else if ((tof1_config & TOF_START) == TOF_START)
//			  VL53L0X_StartMeasurement( &tof1 );
//		  else if ((tof1_config & TOF_MEASURE) == TOF_MEASURE){
//			  VL53L0X_GetMeasurementDataReady(&tof1, &tof1_data_ready);
//			  if(tof1_data_ready == 0x1){
//				  	VL53L0X_GetRangingMeasurementData(&tof1, &tof1_data);
//					sprintf( (char*)buffer, "VL:%d, %d, %.2f, %.2f\n",tof1_data.RangeStatus, tof1_data.RangeMilliMeter,
//							  ( tof1_data.SignalRateRtnMegaCps / 65536.0 ), tof1_data.AmbientRateRtnMegaCps / 65336.0 );
//					HAL_UART_Transmit(&huart2, buffer, strlen((char*)buffer), 200);
//					VL53L0X_ClearInterruptMask(&tof1, VL53L0X_REG_SYSTEM_INTERRUPT_CLEAR);
//			  }
//		  }
//		  tof1_config = TOF_CONFIG_NONE;
//	  }
	  HAL_Delay(1000);
	  HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_TIM1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLK_PLLCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 64 - 1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 20000 - 1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 1500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TOF_RESET_GPIO_Port, TOF_RESET_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : BLUE_BUTTON_Pin TOF_EXTI_Pin */
  GPIO_InitStruct.Pin = BLUE_BUTTON_Pin|TOF_EXTI_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : TOF_RESET_Pin */
  GPIO_InitStruct.Pin = TOF_RESET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(TOF_RESET_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
/* INTERRUPTS */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (buffer[buffer_counter] == '\r' || buffer[buffer_counter] == '\n') {
		buffer[buffer_counter] = '\0';
		command_parser(buffer);

		//clear buffer
		for (uint8_t i = 0; i <= buffer_counter; i++)
			buffer[i] = '\0';

		HAL_UART_Receive_IT(&huart2, buffer, 1);
		buffer_counter = 0;
	}
	else {
		buffer_counter++;
		HAL_UART_Receive_IT(&huart2, buffer + buffer_counter, 1);
	}
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart){
	__HAL_UART_CLEAR_IT(huart, UART_CLEAR_OREF);
	__HAL_UART_CLEAR_IT(huart, UART_CLEAR_FEF);
	HAL_UART_Receive_IT(&huart2, buffer, 1);
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	tx_flag = 0;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	switch(GPIO_Pin){
		case TOF_EXTI_Pin:
			HAL_UART_Transmit( &huart2, (uint8_t *)"GPIO: TOF\n", 10, 100);
			break;
		default:
			break;
	}
}

/* GENRAL FUNCTIONS */
void command_parser(uint8_t *buffer){
	const char cmd_servo_left[] = "servo|LFT";
	const char cmd_servo_right[] = "servo|RGT";
	const char cmd_servo_stop[] = "servo|STP";

//	const char cmd_tof_init[] = "tof|INI";
//
//	const char cmd_tof_accuracy[] = "tof|ACC";
//	const char cmd_tof_speed[] = "tof|SPD";
//	const char cmd_tof_distance[] = "tof|DST";
//
//	const char cmd_tof_continous[] = "tof|CNT";
//	const char cmd_tof_single[] = "tof|SNG";
//
//	const char cmd_tof_start[] = "tof|STR";
	const char cmd_tof_measure[] = "tof|MES";

	if (memcmp(buffer, cmd_servo_left, strlen(cmd_servo_left)) == 0) { //servo left
		htim1.Instance->CCR3 = SERVO_LEFT;
		HAL_UART_Transmit(&huart2, (uint8_t *)"0x01\n", 5, 100);
	}
	else if (memcmp(buffer, cmd_servo_right, strlen(cmd_servo_right)) == 0) {//servo right
		htim1.Instance->CCR3 = SERVO_RIGHT;
		HAL_UART_Transmit(&huart2, (uint8_t *)"0x02\n", 5, 100);
	}
	else if (memcmp(buffer, cmd_servo_stop, strlen(cmd_servo_stop)) == 0) {//servo stop
		htim1.Instance->CCR3 = SERVO_STOP;
		HAL_UART_Transmit(&huart2, (uint8_t *)"0x00\n", 5, 100);
	}
//	else if (memcmp(buffer, cmd_tof_init, strlen(cmd_tof_init)) == 0) {//tof init
//		tof_init(&tof1);
//		tof1_config = TOF_INIT;
//	}
//	else if (memcmp(buffer, cmd_tof_accuracy, strlen(cmd_tof_accuracy)) == 0) {//tof high accuracy profile
//		tof_set_range_profile(&tof1, HIGH_ACCURACY);
//		tof1_config = TOF_ACCURACY;
//	}
//	else if (memcmp(buffer, cmd_tof_speed, strlen(cmd_tof_speed)) == 0) {//tof high speed profile
//		tof_set_range_profile(&tof1, HIGH_SPEED);
//		tof1_config = TOF_SPEED;
//	}
//	else if (memcmp(buffer, cmd_tof_distance, strlen(cmd_tof_distance)) == 0) {//tof long range profile
//		tof_set_range_profile(&tof1, LONG_RANGE);
//		tof1_config = TOF_DISTANCE;
//	}
//	else if (memcmp(buffer, cmd_tof_continous, strlen(cmd_tof_continous)) == 0) {//tof continous measurement mode
//		tof_set_device_mode(&tof1, CONTINOUS);
//		tof1_config = TOF_CONTINOUS;
//	}
//	else if (memcmp(buffer, cmd_tof_single, strlen(cmd_tof_single)) == 0) {//tof single measurement mode
//		tof_set_device_mode(&tof1, SINGLE);
//		tof1_config = TOF_SINGLE;
//	}
//	else if (memcmp(buffer, cmd_tof_start, strlen(cmd_tof_start)) == 0){
//		tof1_config = TOF_START;
//	}
	else if (memcmp(buffer, cmd_tof_measure, strlen(cmd_tof_measure)) == 0){
		  VL53L0X_GetMeasurementDataReady(&tof1, &tof1_data_ready);
		  if(tof1_data_ready == 0x1){
			  	VL53L0X_GetRangingMeasurementData(&tof1, &tof1_data);
				sprintf( (char*)buffer, "VL:%d, %d, %.2f, %.2f\n",tof1_data.RangeStatus, tof1_data.RangeMilliMeter,
						  ( tof1_data.SignalRateRtnMegaCps / 65536.0 ), tof1_data.AmbientRateRtnMegaCps / 65336.0 );
				HAL_UART_Transmit(&huart2, buffer, strlen((char*)buffer), 200);
				VL53L0X_ClearInterruptMask(&tof1, VL53L0X_REG_SYSTEM_INTERRUPT_CLEAR);
		  }

//		tof1_config = TOF_MEASURE;
	}
	else
		HAL_UART_Transmit(&huart2, (uint8_t *)"0xFF\n", 5, 100);//no matching command
}

//void tof_set_device_mode(VL53L0X_Dev_t *tof, tof_device_mode mode){
//	VL53L0X_Error status = 0;

//	switch(mode){
//		case CONTINOUS:
//			VL53L0X_SetDeviceMode(tof, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
//			status = VL53L0X_SetDeviceMode(tof, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
//			if (status != VL53L0X_ERROR_NONE)
//				HAL_UART_Transmit(&huart2, (uint8_t *)"0x17\n", 5, 100);
//			break;
//		case SINGLE:
//			VL53L0X_SetDeviceMode(tof, VL53L0X_DEVICEMODE_SINGLE_RANGING);
//			status = VL53L0X_SetDeviceMode(tof, VL53L0X_DEVICEMODE_SINGLE_RANGING);
//			if (status != VL53L0X_ERROR_NONE)
//				HAL_UART_Transmit(&huart2, (uint8_t *)"0x17\n", 5, 100);
//			break;
//		default:
//			break;
//	}
//	HAL_UART_Transmit(&huart2, (uint8_t *)"0x10\n", 5, 100);
//}

//void tof_set_range_profile(VL53L0X_Dev_t *tof, tof_range_profile profile){
//	VL53L0X_Error status = 0;

//	switch(profile){
//		case HIGH_ACCURACY:
//			VL53L0X_SetMeasurementTimingBudgetMicroSeconds(tof, 200000);
//			status = VL53L0X_SetMeasurementTimingBudgetMicroSeconds(tof, 200000);
//			if (status != VL53L0X_ERROR_NONE){
//				HAL_UART_Transmit(&huart2, (uint8_t *)"0x16\n", 5, 100);
//				return;
//			}
//			break;
//		case LONG_RANGE:
//			VL53L0X_SetMeasurementTimingBudgetMicroSeconds(tof, 33000);
//			VL53L0X_SetVcselPulsePeriod(tof, VL53L0X_VCSEL_PERIOD_PRE_RANGE, 18);
//			VL53L0X_SetVcselPulsePeriod(tof, VL53L0X_VCSEL_PERIOD_FINAL_RANGE, 14);
//			status = VL53L0X_SetMeasurementTimingBudgetMicroSeconds(tof, 33000);
//			if (status != VL53L0X_ERROR_NONE){
//				HAL_UART_Transmit(&huart2, (uint8_t *)"0x16\n", 5, 100);
//				return;
//			}
//			status = VL53L0X_SetVcselPulsePeriod(tof, VL53L0X_VCSEL_PERIOD_PRE_RANGE, 18);
//			if (status != VL53L0X_ERROR_NONE){
//				HAL_UART_Transmit(&huart2, (uint8_t *)"0x16\n", 5, 100);
//				return;
//			}
//			status = VL53L0X_SetVcselPulsePeriod(tof, VL53L0X_VCSEL_PERIOD_FINAL_RANGE, 14);
//			if (status != VL53L0X_ERROR_NONE){
//				HAL_UART_Transmit(&huart2, (uint8_t *)"0x16\n", 5, 100);
//				return;
//			}
//			break;
//		case HIGH_SPEED:
//			VL53L0X_SetMeasurementTimingBudgetMicroSeconds(tof, 20000);
//			status = VL53L0X_SetMeasurementTimingBudgetMicroSeconds(tof, 20000);
//			if (status != VL53L0X_ERROR_NONE){
//				HAL_UART_Transmit(&huart2, (uint8_t *)"0x16\n", 5, 100);
//				return;
//			}
//			break;
//		default:
//			break;
//	}
//	HAL_UART_Transmit(&huart2, (uint8_t *)"0x10\n", 5, 100);
//}

//void tof_init(VL53L0X_Dev_t *tof){
//	uint32_t spadCount;
//	uint8_t isAperture, VhvSettings, phaseCall;
//
//	VL53L0X_Error status = 0;
//
//	tof->I2cDevAddr = 0x52;
//	VL53L0X_DataInit(tof);
//	VL53L0X_StaticInit(tof);
//	VL53L0X_PerformRefSpadManagement(tof, &spadCount, &isAperture);
//	VL53L0X_PerformRefCalibration(tof, &VhvSettings, &phaseCall);
//	status = VL53L0X_DataInit(tof);
//	if (status != VL53L0X_ERROR_NONE){
//		HAL_UART_Transmit(&huart2, (uint8_t *)"0x12\n", 5, 100);
//		return;
//	}
//	status = VL53L0X_StaticInit(tof);
//	if (status != VL53L0X_ERROR_NONE){
//		HAL_UART_Transmit(&huart2, (uint8_t *)"0x13\n", 5, 100);
//		return;
//	}
//	status = VL53L0X_PerformRefSpadManagement(tof, &spadCount, &isAperture);
//	if (status != VL53L0X_ERROR_NONE){
//		HAL_UART_Transmit(&huart2, (uint8_t *)"0x14\n", 5, 100);
//		return;
//	}
//	status = VL53L0X_PerformRefCalibration(tof, &VhvSettings, &phaseCall);
//	if (status != VL53L0X_ERROR_NONE){
//		HAL_UART_Transmit(&huart2, (uint8_t *)"0x15\n", 5, 100);
//		return;
//	}
//	HAL_UART_Transmit(&huart2, (uint8_t *)"0x10\n", 5, 100);
//}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
