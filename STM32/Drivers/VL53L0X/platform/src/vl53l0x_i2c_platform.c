#include "vl53l0x_i2c_platform.h"
#include "stm32f3xx_hal.h"

extern I2C_HandleTypeDef hi2c1;

int32_t VL53L0X_write_multi(uint8_t address, uint8_t index, uint8_t  *pdata, int32_t count){
    return HAL_I2C_Mem_Write(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, pdata, count, 100);
}

int32_t VL53L0X_read_multi(uint8_t address,  uint8_t index, uint8_t  *pdata, int32_t count){
	return HAL_I2C_Mem_Read(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, pdata, count, 100);
}

int32_t VL53L0X_write_byte(uint8_t address,  uint8_t index, uint8_t   data){
	return HAL_I2C_Mem_Write(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, &data, 1, 100);
}

int32_t VL53L0X_write_word(uint8_t address,  uint8_t index, uint16_t  data){
	return HAL_I2C_Mem_Write(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, &data, 2, 100);
}

int32_t VL53L0X_read_byte(uint8_t address,  uint8_t index, uint8_t  *pdata){
	return HAL_I2C_Mem_Read(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, pdata, 1, 100);
}

int32_t VL53L0X_read_word(uint8_t address,  uint8_t index, uint16_t *pdata){
	return HAL_I2C_Mem_Read(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, pdata, 2, 100);
}

int32_t VL53L0X_read_dword(uint8_t address, uint8_t index, uint32_t *pdata){
	return HAL_I2C_Mem_Read(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, pdata, 4, 100);
}

int32_t VL53L0X_write_dword(uint8_t address, uint8_t index, uint32_t  data){
	return HAL_I2C_Mem_Write(&hi2c1, address, index, I2C_MEMADD_SIZE_8BIT, &data, 4, 100);
}
