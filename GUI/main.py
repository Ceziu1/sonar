import serial.tools.list_ports
import tkinter
import time

from radar import Radar
from enums import Direction
from exceptions import CWRotationLimitException, CCWRotationLimitException
from servo import sev

window = tkinter.Tk()
direction = Direction.STOP

com_ports_list = [i.device for i in serial.tools.list_ports.comports()]
com_ports_selected = tkinter.StringVar()
com_ports_selected.set(com_ports_list[0])

def refresh_com_ports_list():
    com_ports_list = [i.device for i in serial.tools.list_ports.comports()]
    com_ports_menu.children['menu'].delete(0,tkinter.END)
    for i in com_ports_list:
        com_ports_menu.children['menu'].add_command(label=i,command=None)

def select_serial_port(port):
    com_ports_selected.set(port)

def servo(dd):
    global direction
    if dd == Direction.CW: #RIGHT
        sev.write(b'servo|RGT\n')
        direction = Direction.CW
    elif dd == Direction.CCW: #LEFT
        sev.write(b'servo|LFT\n')
        direction = Direction.CCW
    elif dd == Direction.STOP:
        sev.write(b'servo|STP\n')
        direction = Direction.STOP

def connect():
    sev.baudrate = 38400
    sev.port = com_ports_selected.get()
    sev.open()

r = Radar(
    window, 
    bg='black', 
    width=960, 
    height=960)
r.grid(column=0,row=0,rowspan=50)

#com ports list

tkinter.OptionMenu(
    window, 
    com_ports_selected, 
    *com_ports_list, 
    command=select_serial_port).grid(column=1, row=10, sticky=tkinter.N)

#refresh com ports list
tkinter.Button(
    window, 
    text="Refresh", 
    command=refresh_com_ports_list).grid(column=2, row=10, sticky=tkinter.N)

#connect button
tkinter.Button(
    window, 
    text="Connect", 
    command=connect).grid(column=3, row=10, sticky=tkinter.N)

tkinter.Label(
    window,
    text="Servo:").grid(column=1, row=11, sticky=tkinter.NW)

tkinter.Button(
    window, 
    text="Left", 
    command=lambda: servo(Direction.CCW)).grid(column=1, row=12, sticky=tkinter.N)

tkinter.Button(
    window, 
    text="Stop", 
    command=lambda: servo(Direction.STOP)).grid(column=2, row=12, sticky=tkinter.N)

tkinter.Button(
    window, 
    text="Right", 
    command=lambda: servo(Direction.CW)).grid(column=3, row=12, sticky=tkinter.N)

#debug output
output = tkinter.Text(
    window, 
    height=10,
    width=50,
    bg='gray',
    state='disable')
output.grid(column=1, row=13, columnspan=3, sticky=tkinter.N)

window.update()

while True:
    try:
        r.rotate(direction)
        print(id(direction))
    except CWRotationLimitException:
        r.stop(direction)
        direction = Direction.CCW
    except CCWRotationLimitException:
        r.stop(direction)
        direction = Direction.CW
    window.update()