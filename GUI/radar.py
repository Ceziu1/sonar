import math

from tkinter import Canvas
from enums import Direction
from exceptions import CWRotationLimitException, CCWRotationLimitException
from PIL import Image, ImageTk
from servo import sev

map_value = lambda value, min_old, max_old, min_new, max_new: min_new + (value - min_old) * (max_new - min_new) / (max_old - min_old)

class Radar(Canvas):
    
    def __init__(self, master, **options):
        Canvas.__init__(self, master, **options)
        self.__objects = []
        self.beam_start = 30
        self.beam_width = 1
        self.echo_start = self.beam_start + self.beam_width
        self.echo_width = 45

        self.__draw_echo()

        self.create_oval(110, 110, int(self['height'])-110, int(self['height'])-110, outline='#288039', width=2)
        self.create_oval(210, 210, int(self['height'])-210, int(self['height'])-210, outline='#288039', width=2)
        self.create_oval(310, 310, int(self['height'])-310, int(self['height'])-310, outline='#288039', width=2)
        self.create_oval(410, 410, int(self['height'])-410, int(self['height'])-410, outline='#288039', width=2)

        self.create_line(
            60,
            int(self['height'])/2,
            int(self['height'])-60,
            int(self['height'])/2,
            width=2,
            fill='#288039'
        )
        self.create_line(
            int(self['height'])/2,
            60,
            int(self['height'])/2,
            int(self['height'])-60,
            width=2,
            fill='#288039'
        )

    def draw_object(self, distance):
        #start drawing from here
        x1 = abs(math.sin(-(math.pi * (self.beam_start-90) / 180)) * distance + int(self['height'])/2)
        y1 = abs(math.cos(math.pi * (self.beam_start-90) / 180) * distance - int(self['height'])/2)
        #end is straight line to the end of radar GUI
        x2 = abs(math.sin(-(math.pi * (self.beam_start-90) / 180)) * (370 - distance) + x1)
        y2 = abs(math.cos(math.pi * (self.beam_start-90) / 180) * (370 - distance) - y1)
        self.__objects.append(self.create_line(x1, y1, x2, y2, fill='#ff0000', width=2))
        for id in self.__objects:
            current_color = self.itemcget(id, 'fill')
            if current_color == '#000000':
                self.delete(id)
                self.__objects.pop(0)
                continue
            new_color = '#' + hex(int(current_color[1:3],16) - 3)[2:]#17 * 5 * 3!!!! = 255
            if len(new_color) == 2:
                new_color = '#0' + new_color[-1] 
            self.itemconfig(id, fill=new_color + '0000')

    def __draw_echo(self):
        self.beam = self.create_arc(
            110,
            110,
            int(self['height'])-110,
            int(self['height'])-110,
            start=self.beam_start,
            extent=self.beam_width,
            fill='#4EFF70'
        )
        self.echo = self.create_arc(
            110,
            110,
            int(self['height'])-110,
            int(self['height'])-110,
            start=self.echo_start,
            extent=self.echo_width,
            fill='#9CFFAF'
        )

    def stop(self, direction):
        if isinstance(direction, Direction) is not True:
            raise ValueError("Unsupported direction")
        while self.echo_width > 0:
            if direction == Direction.CCW:
                self.echo_start += 1
            self.echo_width -= 1
            self.delete(self.echo)
            self.delete(self.beam)
            self.__draw_echo()
            self.tag_lower(self.beam)
            self.tag_lower(self.echo)
            self.update()
        self.echo_start -= 45

    def rotate(self, dir):
        # if isinstance(dir, Direction) is not True:
        #     raise ValueError("Unsupported direction")
        if dir == Direction.CW:
            if self.beam_start == 0:
                raise CWRotationLimitException
            self.beam_start -= 1
            if self.echo_width < 45:
                self.echo_width += 1
            self.echo_start = self.beam_start + self.beam_width
        elif dir == Direction.CCW:
            if self.beam_start == 360:
                raise CCWRotationLimitException
            self.beam_start += 1
            if self.echo_width < 45:
                self.echo_width += 1
            self.echo_start = self.beam_start - self.echo_width
        if dir != Direction.STOP:
            sev.write(b'tof|MES\n')
            line = sev.readline()
            # #370 -> radius
            self.draw_object(map_value(300,0,1200,0,370))
            # self.draw_object(map_value(1200,0,1200,0,370))
            self.delete(self.echo)
            self.delete(self.beam)
            self.__draw_echo()
            self.tag_lower(self.beam)
            self.tag_lower(self.echo)